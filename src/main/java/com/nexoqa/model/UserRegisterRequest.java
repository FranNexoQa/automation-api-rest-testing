package com.nexoqa.model;

public class UserRegisterRequest {

    private final String email;
    private final String password;

    public UserRegisterRequest(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

}
