package com.nexoqa.steps;

import com.nexoqa.model.UserRegisterRequest;
import com.nexoqa.model.UserRequest;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import org.apache.http.HttpStatus;
import org.hamcrest.Matchers;

import java.util.List;

import static org.hamcrest.Matchers.is;

public class UserSteps {

    private UserRequest userRequest;
    private UserRegisterRequest userRegisterRequest;

    @Given("^the client wants to obtains the info of the specific user$")
    public void theClientWantsRequestUserById() {
        SerenityRest.given()
                .baseUri("https://reqres.in/api");
    }

    @Given("^the client wants to create a new user$")
    public void theClientWantsCreateUser(List<String> userData) {

        userRequest = new UserRequest(
                userData.get(0),
                userData.get(1)
        );

        SerenityRest.given()
                .header("Content-Type", "application/json")
                .body(userRequest)
                .baseUri("https://reqres.in/api");
    }

    @Given("the client wants to register a specific user")
    public void theClientWantsToRegisterASpecificUser(List<String> userRegisterData) {

        userRegisterRequest = new UserRegisterRequest(
                userRegisterData.get(0),
                userRegisterData.get(1)
        );

        SerenityRest.given()
                .header("Content-Type", "application/json")
                .body(userRegisterRequest)
                .baseUri("https://reqres.in/api");
    }

    @When("^the client request with the GET method the user by ([0-9]*)$")
    public void theClientRequestTheUserInfo(int userId) {
        SerenityRest.when().get("/users/" + userId);
    }

    @When("^the client request with the POST method the user creation$")
    public void theClientRequestTheUserCreation() {
        SerenityRest.when().post("/users");
    }

    @When("the client request with the POST method the user registration")
    public void theClientRequestWithThePOSTMethodTheUserRegistration() {
        SerenityRest.when().post("/register");
    }


    @Then("^the API should be response the user data with the (.*)$")
    public void theAPIResponseTheRequestedUser(String userEmail) {
        SerenityRest.restAssuredThat(response ->
                response.statusCode(HttpStatus.SC_OK)
                        .body("data.email", Matchers.is(userEmail)));
    }

    @Then("^the API should be response with the user data requested$")
    public void theAPIShouldResponseTheUserDataRequested() {
        SerenityRest.restAssuredThat(response ->
                response.statusCode(HttpStatus.SC_CREATED)
                        .body("name", Matchers.is(userRequest.getName()))
                        .body("job", Matchers.is(userRequest.getJob()))
        );
    }

    @Then("^the API (should|shouldn't) be response with the authentication token$")
    public void theAPIShouldBeResponseWithTheAuthenticationToken(String condition) {

        if ("should".equals(condition)) {
            SerenityRest.restAssuredThat(response ->
                    response.statusCode(HttpStatus.SC_OK)
                            .body("id", Matchers.notNullValue())
                            .body("token", Matchers.notNullValue())
            );
        } else {
            SerenityRest.restAssuredThat(response ->
                    response.statusCode(HttpStatus.SC_BAD_REQUEST)
                            .body("id", Matchers.nullValue())
                            .body("token", Matchers.nullValue())
                            .body("error", is("Missing password"))
            );
        }

    }
}
