Feature: User API features

  Scenario Outline: Obtain the correct user data when is requested by id
    Given the client wants to obtains the info of the specific user
    When the client request with the GET method the user by <id>
    Then the API should be response the user data with the <email>

    Examples: Valid user data
      | id | email                    |
      | 2  | janet.weaver@reqres.in   |
      | 7  | michael.lawson@reqres.in |


  Scenario Outline: Create a new user with the correctly data
    Given the client wants to create a new user
      | <name> | <job> |
    When the client request with the POST method the user creation
    Then the API should be response with the user data requested

    Examples: Valid user data
      | name | job               |
      | Pepe | Quality Assurance |
      | Paco | Developer         |


  Scenario Outline: Register a user successfully
    Given the client wants to register a specific user
      | <email> | <password> |
    When the client request with the POST method the user registration
    Then the API should be response with the authentication token

    Examples: Valid user data
      | email              | password |
      | eve.holt@reqres.in | pistol   |


  Scenario Outline: Register a user unsuccessfully
    Given the client wants to register a specific user
      | <email> | <password> |
    When the client request with the POST method the user registration
    Then the API shouldn't be response with the authentication token

    Examples: Valid user data
      | email              | password |
      | eve.holt@reqres.in |          |
